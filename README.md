<h2>Welcome</h2>
Welcome to Magento 2 installation! We're glad you chose to install Magento 2, a cutting-edge, feature-rich eCommerce solution that gets results.

## Magento system requirements
[Magento system requirements](https://devdocs.magento.com/guides/v2.3/install-gde/system-requirements2.html).

## Install Magento

*	[Installation guide](https://devdocs.magento.com/guides/v2.3/install-gde/bk-install-guide.html).

<h2>Contributing to the Magento 2 code base</h2>
Contributions can take the form of new components or features, changes to existing features, tests, documentation (such as developer guides, user guides, examples, or specifications), bug fixes, optimizations, or just good suggestions.

To learn about how to make a contribution, click [here][1].

To learn about issues, click [here][2]. To open an issue, click [here][3].

To suggest documentation improvements, click [here][4].

[1]: <https://devdocs.magento.com/guides/v2.3/contributor-guide/contributing.html>
[2]: <https://devdocs.magento.com/guides/v2.3/contributor-guide/contributing.html#report>
[3]: <https://github.com/magento/magento2/issues>
[4]: <https://devdocs.magento.com>